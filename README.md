# Sensor de distancia ultrasónico

<img src="https://i.ebayimg.com/images/g/3PoAAOSwf1xcWTsQ/s-l300.jpg" alt="fig. 1" width="300"/>

## Descripción

El sensor ultrasónico HC-S04 mide distancia enviando un pulso de ultrasonido (40kHz) y midiendo
el tiempo que tarda dicho pulso en ir y volver al rebotar contra un objeto rígido.

<img src="img/physics.png" alt="fig. 1" width="450"/>

## Especificaciones
- Resolución: 0.3cm
- Ángulo: < 15°
- Rango: 2cm - 450cm

El módulo tiene 4 terminales (pines) de conexión:
- 1. VCC: conectar a +5V
- 2. TRIG: señal para iniciar una medición de distancia
- 3. ECHO: señal que indica la detección de un rebote
- 4. GND: conectar a GND

## Diagrama de conexión con Arduino

<img src="img/Schematic.png" alt="fig. 1" width="450"/>

## Carga del firmware

Esta aplicación es muy sencilla. Lee los datos del sensor y los envía a la computadora para visualizarlos.

1. Descargar a su PC el [firmware](firmware/firmware.ino)
2. Abrir la aplicación [Arduino IDE](https://www.arduino.cc/en/software)

<img src="img/arduinoIDE_1.png" alt="fig. 1" width="300"/>

3. Abrir el código que descargó en el punto 1.
4. Conectar el Arduino a la computadora con el cable USB.
5. Seleccionar el puerto serie correspondiente al Arduino.

<img src="img/arduinoIDE_2.png" alt="fig. 1" width="300"/>

6. Seleccionar la placa Arduino que corresponde.

<img src="img/arduinoIDE_3.png" alt="fig. 1" width="300"/>

7. Cargar el firmware.
8. Abrir el *monitor serial* o el *serial plotter*

<img src="img/arduinoIDE_4.png" alt="fig. 1" width="300"/>

9. Asegurarse que la velocidad de comunicación está seteada en 9600 bauds

### AHORA ACERCÁ Y ALEJÁ UN OBJETO AL SENSOR!!!
