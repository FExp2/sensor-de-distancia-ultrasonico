// ---------------------------------------------------------------- //
// Arduino Ultrasoninc Sensor HC-SR04
// ---------------------------------------------------------------- //

#define echoPin 2 // Conectar el pin D2 de Arduino al pin ECHO
#define trigPin 3 // Conectar el pin D3 de Arduino al pin TRIG

//variables
long duration; // tiempo que tarda el echo en volver
int distance; // distancia medida

void setup() {
  pinMode(trigPin, OUTPUT); // Define el pin conectado a TRIG como salida
  pinMode(echoPin, INPUT); // Define el pin conectado a ECHO como entrada
  Serial.begin(9600); // Iniciamos el puerto serie a 9600 bauds.
}
void loop() {
  // Ponemos el pin de Trigger en cero (bajo)
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2); //Pausa de 2 microsegundos
  // Activamos el pin de Trigger por 10 microsegundos
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW); //Desactivamos el pin de Trigger
  // Leemos el tiempo (ms) que tarda el echo en volver
  duration = pulseIn(echoPin, HIGH);
  // Calculamos la distancia (asumiendo que la velocidad del sonido es 0.034m/ms)
  distance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)
  // Enviamos la informacion a la PC
  Serial.print("Distancia: ");
  Serial.print(distance);
  Serial.println(" cm");
}
